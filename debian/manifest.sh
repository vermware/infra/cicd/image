#!/bin/sh

set -eu

arches='amd64 ppc64le'
os=linux

oldstable=bullseye
stable=bookworm
testing=trixie

# Only run on the default branch.
[ "$CI_COMMIT_REF_NAME" = "$CI_DEFAULT_BRANCH" ] || exit 0

manifest()
(
	codename="$1"

	for tag in "$@"; do
		# Word splitting is desired here, no quotes.
		for arch in $arches; do
			docker manifest create -a -- \
				"$CI_REGISTRY_IMAGE/debian:$tag" \
				"$CI_REGISTRY_IMAGE/debian:$codename-$arch-$os"

			docker manifest annotate --arch "$arch" --os "$os" -- \
				"$CI_REGISTRY_IMAGE/debian:$tag" \
				"$CI_REGISTRY_IMAGE/debian:$codename-$arch-$os"
		done

		docker manifest push -p -- "$CI_REGISTRY_IMAGE/debian:$tag"
	done

	exit 0
)

manifest "$oldstable" oldstable
manifest "$stable" stable latest
manifest "$testing" testing
