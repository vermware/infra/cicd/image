#!/bin/sh

set -eu

os=linux

codename="$1"
arch="$2"

docker build -t "$CI_REGISTRY_IMAGE/debian:$codename-$arch-$os" -- .

# Only continue with pushing for the default branch.
[ "$CI_COMMIT_REF_NAME" = "$CI_DEFAULT_BRANCH" ] || exit 0

docker push -- "$CI_REGISTRY_IMAGE/debian:$codename-$arch-$os"
