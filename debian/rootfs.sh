#!/bin/sh

set -eu

export DEBIAN_FRONTEND=noninteractive
export TZ=UTC0

codename="$1"

# Use pipeline -7 days' date to avoid broken archives during upstream sync.
# This requires the GNU implementation for relative date strings support.
date="$(date -d "$CI_PIPELINE_CREATED_AT -7 days" -Iseconds)"

debuerreotype-init rootfs "$codename" "$date"
debuerreotype-minimizing-config rootfs
debuerreotype-apt-get rootfs update -q
debuerreotype-apt-get rootfs dist-upgrade -qy
debuerreotype-debian-sources-list rootfs "$codename"
debuerreotype-tar rootfs rootfs.tar

rm -r rootfs
